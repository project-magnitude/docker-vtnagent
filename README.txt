=====================================================
Dockerized VTN agent implementation - v1.0
=====================================================


.:: CONTENTS ::.

This readme allows the installation of the VTN agent released through Docker.


.:: SETUP ::.

BUILD AND RUN DOCKER IMAGES

Execute in the folder 'docker-vtnagent' the command for setting up Docker compose:
	docker-compose up


.:: CHANGELOG ::.

v1.0 Tested on the development machine


.:: CONTACTS ::.

alessio.roppolo@eng.it